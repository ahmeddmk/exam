FROM openjdk:8-jdk-alpine
#ARG JAR_FILE=target/*.jar
COPY target/*.jar eureka.jar
EXPOSE 8761

ENTRYPOINT ["java","-jar","/eureka.jar"]
